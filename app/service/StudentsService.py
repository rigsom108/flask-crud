from flask import Flask, request,jsonify
import json
from werkzeug.wrappers import response
from app.model.Models import Student, db
from sqlalchemy import create_engine
from app import con

class StudentDetails:
	
    def createDetails(data):
        details = student(
            name = data["name"],
            course = data["course"],
            section = data["section"],
        ) 

        db.session.add(details)
        db.session.commit()
        # response = request.json
        # print("HHHHHHHHHHHHHHHHHHH",response)
        return jsonify({
            "response_code": "200",
            "response_message": "success"
        })
        

    def updateDetails(id,name,course,section):
        Update = student.query.filter_by(id=id).first()
        Update.name = name
        Update.course = course
        Update.section = section
        db.session.commit()

        return jsonify({
            "response_code": "200",
            "response_message": "success"
        })
        
    def deleteDetail(id):
        student.query.filter_by(id=id).delete()
        db.session.commit()

        return jsonify({
            "response_code": "200",
            "response_message": "success"
        })
        

    def viewDetails():
        eng = create_engine(con)
        connection = eng.connect()
        data = connection.execute("SELECT * FROM student")
        return jsonify([dict(row) for row in data])
        

student = Student