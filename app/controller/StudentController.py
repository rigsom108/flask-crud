from flask import Flask, request, jsonify
from app import app
from app.model.Models import Student
from app.service.StudentsService import StudentDetails


class StudentController:

    @app.route('/add', methods=['POST'])
    def add():
        response = request.json
        return studentDetailsObj.createDetails(response)


    @app.route('/view', methods=["GET"])
    def view():
        return studentDetailsObj.viewDetails()
    
    @app.route('/update', methods= ["PUT"])
    def update():
        response = request.json
        return studentDetailsObj.updateDetails(response['id'],response['name'],response['course'],response['section'])

    @app.route('/delete/<id>',methods=['DELETE'])
    def delete(id):
        return studentDetailsObj.deleteDetail(id)

studentDetailsObj = StudentDetails
